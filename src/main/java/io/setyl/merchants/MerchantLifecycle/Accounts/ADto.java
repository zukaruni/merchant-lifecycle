package io.setyl.merchants.MerchantLifecycle.Accounts;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class ADto {

    private String method;
    private String number;
    private String routing;
}
