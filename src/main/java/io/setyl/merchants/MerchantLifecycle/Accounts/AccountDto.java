package io.setyl.merchants.MerchantLifecycle.Accounts;

import lombok.Data;

import javax.persistence.Embeddable;



@Embeddable
@Data
public class AccountDto {
    private String primary;
    private ADto account;
}
