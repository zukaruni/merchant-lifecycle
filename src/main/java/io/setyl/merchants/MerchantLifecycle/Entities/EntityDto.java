package io.setyl.merchants.MerchantLifecycle.Entities;

import io.setyl.merchants.MerchantLifecycle.Accounts.AccountDto;
import lombok.Data;

import javax.persistence.Embeddable;
import java.util.List;

@Embeddable
@Data
public class EntityDto {

    private String type;
    private String name;
    private String address1;
    private String city;
    private String state;
    private String zip;
    private String country;
    private String phone;
    private String email;
    private String ein;
    private String Public;
    private String website;
    private List<AccountDto> accounts;
}
