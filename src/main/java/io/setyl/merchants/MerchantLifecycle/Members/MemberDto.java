package io.setyl.merchants.MerchantLifecycle.Members;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class MemberDto {

    private String title;
    private String first;
    private String last;
    private String dob;
    private String ownership;
    private String email;
    private String ssn;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String country;
    private String timezone;
    private String dl;
    private String dlstate;
    private String primary;
}
