package io.setyl.merchants.MerchantLifecycle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MerchantLifecycleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MerchantLifecycleApplication.class, args);
	}
}
