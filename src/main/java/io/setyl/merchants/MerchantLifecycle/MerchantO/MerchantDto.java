package io.setyl.merchants.MerchantLifecycle.MerchantO;

import io.setyl.merchants.MerchantLifecycle.Entities.EntityDto;
import io.setyl.merchants.MerchantLifecycle.Members.MemberDto;
import lombok.Data;

import javax.persistence.Embeddable;
import java.util.List;



@Embeddable
@Data
public class MerchantDto {

    private String New;
    private String established;
    private String mcc;
    private String status;
    private EntityDto entity;
    private List<MemberDto> members;


}
