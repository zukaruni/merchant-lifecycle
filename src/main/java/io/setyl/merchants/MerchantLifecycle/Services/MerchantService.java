package io.setyl.merchants.MerchantLifecycle.Services;

import io.setyl.merchants.MerchantLifecycle.MerchantO.MerchantDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MerchantService {

    private RestTemplate restTemplate;
    private String baseUrl;
    private String merchantLink;
    private HttpHeaders httpHeaders;


    @Autowired
    public MerchantService(RestTemplate restTemplate, @Value("https://test-api.splashpayments.com") String baseUrl, HttpHeaders httpHeaders) {
        this.baseUrl = baseUrl;
        this.merchantLink = baseUrl + "/merchants";
        this.restTemplate = restTemplate;
        this.httpHeaders = httpHeaders;
    }

    public String onboardMerchant(MerchantDto merchantDto){

        httpHeaders.set("APIKEY", "f64b8c5a0d544d9997dc21a74b2afbee");
        httpHeaders.set("Content-Type", "application/json");


        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpEntity<MerchantDto> request = new HttpEntity<>(merchantDto, httpHeaders);
        String responseEnt =  restTemplate.postForObject(merchantLink, request, String.class);
        return responseEnt;
    }


}
