package io.setyl.merchants.MerchantLifecycle.web.api;

import io.setyl.merchants.MerchantLifecycle.MerchantO.MerchantDto;
import io.setyl.merchants.MerchantLifecycle.Services.MerchantService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/api", produces = "application/json")
public class MerchantController {


    private MerchantService merchantService;

    @Autowired
    public MerchantController(MerchantService merchantService){
        this.merchantService = merchantService;
    }

    @ApiOperation(value = "Create Merchant", nickname = "Onboard a merchant")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/merchant",produces = "application/json")
    public String onboardMerchant(@RequestBody MerchantDto merchantDto) {
        return merchantService.onboardMerchant(merchantDto);
    }


}
